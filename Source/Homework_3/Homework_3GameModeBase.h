// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Homework_3GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class HOMEWORK_3_API AHomework_3GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
